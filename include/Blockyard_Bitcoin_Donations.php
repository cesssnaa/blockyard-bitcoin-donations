<?php

/**
 * Class Blockyard_Bitcoin_Donations
 */
class Blockyard_Bitcoin_Donations {

	/**
	 * Blockyard_Bitcoin_Donations constructor.
	 *
	 * Do this whenever this class is instantiated.
	 *
	 * the uses below is a phpDoc code hint.   It tells an IDE what functions
	 * this method uses when they are not called directly.     WordPress
	 * hooks and filters are ALWAYS an indirect call and thus some IDEs are
	 * going to need some hints for autocomplete and syntax validation.
	 *
	 * @uses \Blockyard_Bitcoin_Donations::add_css_style
	 */
	public function __construct() {

		// When WordPress loads up JavaScript for the page, tell it to add our CSS as well.
		add_action( 'wp_enqueue_scripts', array( $this , 'add_css_style' ) );

		// Register the [donate_bitcoin] shortcode output.
		add_shortcode( 'donate_bitcoin', array( $this , 'display_donation_box' ) );
	}


	/**
	 * Display something on the page.
	 *
	 * Explicitly define our shortcode output method as PUBLIC.
	 *
	 * Public means this code can be run from somewhere outside this class.
	 *
	 * ALL code that is called from a WordPress hook or filter MUST be public.
	 *
	 * If the scope of a function is not declared explicitly it is assumed to be public.
	 *
	 * If you define this as private WordPress will complain.   That is because
	 * the WordPress core code engine -- code that lives OUTSIDE of this class
	 * has to call this function.    If you explicitly mark this method as private
	 * then PHP will complain when some other code outside of this class, "the
	 * "WordPress code", tries to run it.
	 */
	public function display_donation_box() {

		// Replace the hard-coded output with a method in
		// THIS class to generate the stuff we are going to
		// display where the shortcode appears on the page.
		//
		// It makes the code more readable and easily modified.
		//
		// $this is a special variable in PHP when used within a class
		// it points to a method within the class itself.
		//
		// For the uber techies it is actually pointing to a specific
		// INSTANTIATION of this class.
		//
		$output = $this->get_bitcoin_donation_html();

		return $output;
	}

	/**
	 * Generate the output for the BTC donation box.
	 *
	 * private before the function declaration tells PHP that only'
	 * other methods within this class can run this code.
	 *
	 * It is one of several SCOPES -- the other common scope being PUBLIC
	 *
	 * This helps provide security as well as helps phpStorm and other IDEs
	 * tell coders if something is not being used properly or as intended.
	 */
	private function get_bitcoin_donation_html() {

		// get_the_author_meta() is a built-in WordPress function
		// It retrieves additional meta data about a user based on what is set in their profile.
		// We've not done it yet, but eventually we will add some fields where each user
		// on our site will be able to add their bitcoin address to their user profile.

		// Get the author's bitcoin address
		//
		$blockyard_author_bitcoin = get_the_author_meta( 'bitcoin' );

		// Get the author's display name v. login  -- "Lance Cleveland" versus "lcleveland"
		$blockyard_author_name = get_the_author_meta( 'display_name' );

		// Get the name we've set for this WordPress site
		// get_bloginfo() is a built-in WordPress function.
		$blockyard_site_name = get_bloginfo( 'name' );

		// Construct a bitcoin address URL from the pieces above.
		// urlencode() is a PHP function to make URL strings properly formatted with + for space , etc.
		// get_permalink() is a WordPress function to get the full URL to the specific page/post where this shortcode appears.
		$blockyard_author_bitcoin_url = 'bitcoin:'. $blockyard_author_bitcoin.
		                                '?label=' . urlencode($blockyard_site_name) .'%3A%20'.
		                                urlencode($blockyard_author_name) .
		                                '&message=Donation%20for%3A'. urlencode(get_permalink() );

		// Code snarfed from Bitmate's plugin to show the progression.
		//
		// These things are NOT following PHP or WordPress best practices:
		// -- NEVER use inline JavaScript in your code.
		// -- ALWAYS use gettext aka the __() text output if you ever hope to have a multilingual plugin.
		//
		$output ='
			<div class="blockyard-author-donation blockyard-ad-widget" id="blockyard-author-donation blockyard-ad-widget">
				<div id="blockyard-cc-btc" class="blockyard-cc-tabs" style="border: none !important; display:block !important;">
					<div class="blockyard-classic"><strong>Did you like this?</strong><br/>Tip '.$blockyard_author_name.' with Bitcoin</div>
				    <div class="blockyard-qr-code">
				    	<img src="'.plugins_url('include/qrme.php?', BLOCKYARD_BTC_DONATE_FILE).$blockyard_author_bitcoin.'" alt="Scan to Donate Bitcoin to '. $blockyard_author_name .'"/>
				    </div><div class="blockyard-window-detail">
				    <input type="text" value="'.$blockyard_author_bitcoin.'" id="bitcoinAddress" class="blockyard-address" readonly>
				    <div class="blockyard-donate-buttons">
				    	<a href="'.$blockyard_author_bitcoin_url.'" title="Donate Bitcoin to '.$blockyard_author_name.'" class="blockyard-button-donate bm-button-donate-btc"><i class="cf cf-btc"></i> Donate via Installed Wallet</a>
				    	<button onclick="copyBitcoinAddress()" class="blockyard-button-copy" id="buttonBitcoinAddress">Copy</button>
				    </div>
				    </div>
				    <script>
					function copyBitcoinAddress() {
					  var copyText = document.getElementById("bitcoinAddress");
					  copyText.select();
					  document.execCommand("Copy");
					  alert("Copied the Bitcoin Address: " + copyText.value);
					}
					</script>
				</div>
				<script>
					jQuery(\'.blockyard-button-copy\').css(\'display\',\'inline\');
				</script>
			</div>
		';

		return $output;

	}

	/**
	 * Called whenever the WordPress wp_enqueue_scripts is called.
	 *
	 * We defined this in...
	 *
	 * Another phpDoc code hint, the sibling of the @uses hint above -- in this case what other
	 * pieces of code are referencing this.
	 *
	 * @used-by \Blockyard_Bitcoin_Donations::__construct  via wp_enqueue_scripts hook
	 */
	public function add_css_style() {

		// This method tells WordPress to keep track of a CSS stylesheet that we may invoke later.
		//
		// Notice our use of the defined constant from our main loader class
		// This allows other parts of our code like this class that does NOT live in the top-level
		// directory for our plugin to still find the "root" folder for our plugin and build a path to the stylesheet.
		//
		wp_register_style( 'blockyard-bitcoin-donations', plugins_url('include/css/blockyard_bitcoin_donations_style.css', BLOCKYARD_BTC_DONATE_FILE) );

		// This invokes (renders) the stylesheet.
		wp_enqueue_style( 'blockyard-bitcoin-donations' );

		wp_register_style( 'blockyard-crypto-font', plugins_url('include/css/cryptofont.min.css', BLOCKYARD_BTC_DONATE_FILE ) );
		wp_enqueue_style( 'blockyard-crypto-font' );
	}

}
